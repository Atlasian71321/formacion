

package net.techu;

import org.eclipse.jetty.http.MetaData;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.ArrayList;
import java.util.List;

@RestController
public class ProductosV2Controller {

    private ArrayList<Producto> listaProductos;
    {
        listaProductos = null;
    }

    public ProductosV2Controller(){
        listaProductos = new ArrayList<>();
        listaProductos.add(new Producto(1, "PR1", 17.7));
        listaProductos.add(new Producto(2, "PR2", 28.8));
    }

    //Get Listador de productos
    @GetMapping (value = "/v2/productos", produces = "application/json")
    public ResponseEntity<List<Producto>> obtenerListado(){
        System.out.println("Obteniendo Listado v2/productos");
        return ResponseEntity.ok(listaProductos);
    }

    //Get un producto
    @GetMapping (value = "/v2/productos/{id}", produces = "application/json")
    public ResponseEntity<Producto> obtenerProducto (@PathVariable int id){

        Producto resultado = null;
        ResponseEntity<Producto> respuesta = null;

        try{
            resultado = listaProductos.get(id);
            respuesta = new ResponseEntity<>(resultado, HttpStatus.OK);
        }
        catch (Exception exception) {
            respuesta = new ResponseEntity<>(resultado, HttpStatus.NOT_FOUND);
        }
        return respuesta;
    }

    //Post un nuevo producto
    @PostMapping(value = "/v2/productos", produces = "application/json")
    public ResponseEntity<String> addProducto (@RequestBody Producto productoNuevo){
        System.out.println("Estoy en añadir");
        System.out.println(productoNuevo.getId());
        System.out.println(productoNuevo.getNombre());
        System.out.println(productoNuevo.getPrecio());
        listaProductos.add(productoNuevo);
        return new ResponseEntity<>("Producto Creado correctamente", HttpStatus.CREATED);
    }

    //Post con Nombre
    @PostMapping (value = "/v2/productos/{nombre}/{cat}", produces = "application/json")
    public ResponseEntity<String> addProducto (@PathVariable ("nombre") String nombre, @PathVariable("cat") String categoria){
        System.out.println("Voy a añadir el producto con nombre " + nombre + " y categoría " + categoria);
        listaProductos.add(new Producto(99, nombre, 100.5));
        return new ResponseEntity<>("Producto Creado Correctamente", HttpStatus.CREATED);
    }

    //Put con id en el body
    @PutMapping (value = "/v2/productos", produces = "application/json")
    public ResponseEntity<String> updateProducto(@RequestBody Producto cambios){
        ResponseEntity respuesta = null;

        try{
            Producto productoModificar = listaProductos.get(cambios.getId());
            System.out.println("Voy a modificar el producto");
            System.out.println("Precio actual: " + String.valueOf(productoModificar.getPrecio()));
            System.out.println("Precio nuevo: " + String.valueOf(cambios.getPrecio()));
            productoModificar.setNombre(cambios.getNombre());
            productoModificar.setPrecio(cambios.getPrecio());
            respuesta = new ResponseEntity<>("Producto Modificado", HttpStatus.NO_CONTENT);

        }
        catch (Exception es){
            respuesta = new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return respuesta;
    }





}
