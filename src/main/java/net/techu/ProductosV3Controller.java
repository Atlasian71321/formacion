package net.techu;

import net.techu.data.ProductoMongo;
import net.techu.data.ProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

//@SpringBootApplication
public class ProductosV3Controller implements CommandLineRunner {


    @Autowired
    private ProductoRepository repository;

    public static void main(String[] args){
        SpringApplication.run(ProductosV3Controller.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        System.out.println("Prepatando Mongo DB");
        List<ProductoMongo> lista = repository.findAll();
        for (ProductoMongo p:lista) {
            System.out.println(p.toString());
        }
    }


}
