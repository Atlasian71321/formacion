package net.techu;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class ProductosController {

    private ArrayList<String> listaProductos = null;

    public ProductosController() {
        listaProductos = new ArrayList<>();
        listaProductos.add("PR1");
        listaProductos.add("PR2");
    }

    @GetMapping("/productos/{id}")
    public ResponseEntity obtenerProducto(@PathVariable int id) {
        String resultado = null;
        try{
            resultado = listaProductos.get(id);
            //return new ResponseEntity<>(resultado, HttpStatus.OK);
            return ResponseEntity.ok(resultado);
        }
        catch(Exception exception){
            resultado = "Producto no encontrado";
            return new ResponseEntity<>(resultado, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/productos", produces = "application/json")
    public ResponseEntity<List<String>> obtenerListado()
    {
        System.out.println("Estoy en obtener");
        return ResponseEntity.ok(listaProductos);
    }

    @PostMapping(value = "/productos", produces="application/json")
    public ResponseEntity<String> addProducto() {
        System.out.println("Estoy en añadir");
        listaProductos.add("NUEVO");
        return new ResponseEntity<>("Producto creado", HttpStatus.CREATED);
    }

    @PostMapping(produces="application/json", value = "/productos/{nom}/{cat}")
    public ResponseEntity<String> addProductoConNombre(@PathVariable("nom") String nombre, @PathVariable("cat") String categoria) {
        System.out.println("Voy a añadir el producto con nombre " + nombre + " y categoría " + categoria);
        listaProductos.add(nombre);
        return new ResponseEntity<>("Producto creado", HttpStatus.CREATED);
    }

    @PutMapping (value = "/productos/{id}/{producto}", produces = "application/json")
    public ResponseEntity<String> updateProducto(@PathVariable int id, @PathVariable String producto){
        try {
            listaProductos.set(id, producto);
            return new ResponseEntity<>("Modificado", HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>("No Modificado", HttpStatus.NOT_FOUND);
        }
    }
}
